stages:
  - build
  - test
  - verify

variables:
  TEST_IMAGE: registry.gitlab.com/gitlab-org/gitlab-development-kit:latest
  DOCKER_VERSION: "19.03.0"

workflow:
  rules:
    # For merge requests, create a pipeline.
    - if: '$CI_MERGE_REQUEST_IID'
    # For `master` branch, create a pipeline (this includes on schedules, pushes, merges, etc.).
    - if: '$CI_COMMIT_BRANCH == "master"'
    # For tags, create a pipeline.
    - if: '$CI_COMMIT_TAG'

default:
  image: $TEST_IMAGE
  tags:
    - gitlab-org

.use-docker-in-docker:
  image: docker:${DOCKER_VERSION}
  services:
    - docker:${DOCKER_VERSION}-dind
  variables:
    DOCKER_DRIVER: overlay2
    DOCKER_HOST: tcp://docker:2375
    DOCKER_TLS_CERTDIR: ""
  tags:
    # See https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/7019 for tag descriptions
    - gitlab-org-docker

.docs-changes: &docs-changes
  - "**/*.{md}"
  - "{doc}/**/*"

.code-changes: &code-changes
  - "**/*.{rb,erb,sh,yml,example,types}"
  - "{lib,gem,bin}/**/*"
  - "{spec,support}/**/*"
  - ".gitlab-ci.yml"
  - "Gemfile{,.lock}"
  - "Makefile"
  - "Rakefile"
  - "Dockerfile"
  - "**/*/Dockerfile"
  - "packages.txt"
  - "**/*/Makefile"
  - "Vagrantfile"
  - "gitlab-openldap/**/*"
  - "vagrant/assets/**/*"

.docs-code-changes: &docs-code-changes
  - "**/*.{md}"
  - "{doc}/**/*"
  - "**/*.{rb,erb,sh,yml,example,types}"
  - "{lib,gem,bin}/**/*"
  - "{spec,support}/**/*"
  - ".gitlab-ci.yml"
  - "Gemfile{,.lock}"
  - "Rakefile"
  - "Dockerfile"
  - "**/*/Dockerfile"
  - "packages.txt"
  - "**/*/Makefile"
  - "Vagrantfile"
  - "gitlab-openldap/**/*"
  - "vagrant/assets/**/*"

.rules:docs-changes:
  rules:
    - changes: *docs-changes

.rules:code-changes:
  rules:
    - changes: *code-changes

.rules:docs-code-changes:
  rules:
    - changes: *docs-code-changes

.rules:docs-code-changes-manual:
  rules:
    - changes: *docs-code-changes
      when: manual
      allow_failure: true

build:image:
  extends:
    - .rules:docs-code-changes-manual
    - .use-docker-in-docker
  image: docker:git
  stage: build
  script:
    # taken from https://gitlab.com/gitlab-org/gitlab-qa/blob/master/.gitlab-ci.yml
    - ./bin/docker load
    - ./bin/docker build
    - ./bin/docker store
    - test -n "$CI_BUILD_TOKEN" || exit 0
    - ./bin/docker publish
  cache:
      key: "docker-build-cache"
      paths:
        - ./latest_image.tar

test:docs_lint:
  extends: .rules:docs-changes
  image: registry.gitlab.com/gitlab-org/gitlab-docs:lint
  stage: test
  script:
    - vale --minAlertLevel error doc
    - markdownlint --config .markdownlint.json 'doc/**/*.md'

test:static-analysis:
  extends: .rules:docs-code-changes
  image: registry.gitlab.com/gitlab-org/gitlab-build-images:node-10
  stage: test
  script:
    - apt-get update && apt-get install -y rake
    - npm config set unsafe-perm true # enable run npm as root
    - make static-analysis

test:rspec:
  extends: .rules:code-changes
  image: ruby:2.6
  stage: test
  script:
    - bundle install --jobs 4
    - bundle exec rspec --format progress --format RspecJunitFormatter --out rspec.xml
  artifacts:
    paths:
      - rspec.xml
    reports:
      junit: rspec.xml

test:code_quality:
  extends:
    - .rules:code-changes
    - .use-docker-in-docker
  stage: test
  allow_failure: true
  script:
    - export SP_VERSION=$(echo "$CI_SERVER_VERSION" | sed 's/^\([0-9]*\)\.\([0-9]*\).*/\1-\2-stable/')
    - docker run
        --env SOURCE_CODE="$PWD"
        --volume "$PWD":/code
        --volume /var/run/docker.sock:/var/run/docker.sock
        "registry.gitlab.com/gitlab-org/security-products/codequality:$SP_VERSION" /code
  artifacts:
    reports:
      codequality: [gl-code-quality-report.json]
    paths:
      - gl-code-quality-report.json

test:container_scanning:
  extends:
    - .rules:code-changes
    - .use-docker-in-docker
  stage: test
  allow_failure: true
  dependencies: []
  script:
    - docker run -d --name db arminc/clair-db:latest
    - docker run -p 6060:6060 --link db:postgres -d --name clair --restart on-failure arminc/clair-local-scan:v2.0.1
    - apk add -U wget ca-certificates
    - docker pull ${TEST_IMAGE}
    - wget https://github.com/arminc/clair-scanner/releases/download/v8/clair-scanner_linux_amd64
    - mv clair-scanner_linux_amd64 clair-scanner
    - chmod +x clair-scanner
    - touch clair-whitelist.yml
    - retries=0
    - echo "Waiting for clair daemon to start"
    - while( ! wget -T 10 -q -O /dev/null http://docker:6060/v1/namespaces ) ; do sleep 1 ; echo -n "." ; if [ $retries -eq 10 ] ; then echo " Timeout, aborting." ; exit 1 ; fi ; retries=$(($retries+1)) ; done
    - ./clair-scanner -c http://docker:6060 --ip $(hostname -i) -r gl-sast-container-report.json -l clair.log -w clair-whitelist.yml ${TEST_IMAGE} || true
  artifacts:
    paths: [gl-sast-container-report.json]

test:shellcheck:
  stage: test
  extends: .rules:code-changes
  image: koalaman/shellcheck-alpine:stable
  script:
    - support/ci/shellcheck

.verify_job:
  stage: verify
  extends: .rules:code-changes
  artifacts:
    paths:
      - ./gitlab-development-kit/gitlab/log/*.log
      - ./gitlab-development-kit/log
    expire_in: 2 days
    when: always
  retry: 1

verify:install:
  extends: .verify_job
  script:
    - support/ci/verify_install

verify:update:
  extends: .verify_job
  script:
    - support/ci/verify_update
